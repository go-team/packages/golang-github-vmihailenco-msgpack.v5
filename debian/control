Source: golang-github-vmihailenco-msgpack.v5
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-stretchr-testify-dev,
               golang-github-vmihailenco-tagparser.v2-dev,
               golang-google-appengine-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-vmihailenco-msgpack.v5
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-vmihailenco-msgpack.v5.git
Homepage: https://github.com/vmihailenco/msgpack
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/vmihailenco/msgpack/v5

Package: golang-github-vmihailenco-msgpack.v5-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-stretchr-testify-dev,
         golang-github-vmihailenco-tagparser.v2-dev,
         golang-google-appengine-dev,
         ${misc:Depends}
Description: MessagePack (msgpack.org) encoding for Golang (library)
 MessagePack encoding for Golang
 .
 Features
 .
  * Primitives, arrays, maps, structs, time.Time and interface{}.
  * Appengine \*datastore.Key and datastore.Cursor.
  * CustomEncoder/CustomDecoder interfaces for custom encoding.
  * Extensions to encode type information.
  * Renaming fields via msgpack:"my_field_name" and alias via
    msgpack:"alias:another_name".
  * Omitting individual empty fields via msgpack:",omitempty" tag or all
    empty fields in a struct.
  * Map keys sorting.
  * Encoding/decoding all structs as arrays or individual structs.
  * Encoder.SetCustomStructTag with Decoder.SetCustomStructTag
    can turn msgpack into drop-in replacement for any tag.
  * Simple but very fast and efficient queries.
